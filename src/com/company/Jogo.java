package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Jogo {
    private int melhorPontuacao = 0, somaPontos = 0;
    HashMap<String, Integer> baralhoCheio = new HashMap<>();
    HashMap<String, Integer> cartasSelecionadas = new HashMap<>();
    IO io = new IO();
    boolean darCarta = true;
    ArrayList<String> chaves = new ArrayList<>();


    public void iniciarJogo() {
        Baralho baralho = new Baralho();
        baralhoCheio = baralho.criarBaralho();
        do {
            retirarCarta(baralhoCheio);
            boolean valendo = validarLimite(somaPontos);
            int resposta;
            if(valendo){
                resposta = io.pergunta();
            } else {
                resposta = 2;
            }
            if (resposta == 2) {
                darCarta = false;
                io.mostrarPontuacaoFinal(somaPontos);
                io.mostrarHistorico(historico(somaPontos));
                int novoJogo = io.jogarDeNovo();
                if (novoJogo == 1) {
                    darCarta = true;
                    limparMao();
                } else if(novoJogo == 2){
                    darCarta = false;
                }
            }
        } while (darCarta);
    }

    public void retirarCarta(HashMap<String, Integer> baralho) {
        Random random = new Random();
        int carta = random.nextInt(baralho.size());
        for (String key : baralho.keySet()) {
            chaves.add(key);
        }
        acumularCartas(carta,baralho);
    }

    public void acumularCartas(int carta, HashMap<String, Integer> baralho){
        cartasSelecionadas.put(chaves.get(carta), baralho.get(chaves.get(carta)));
        int somaPontos = somarPontuacao(baralho.get(chaves.get(carta)));
        baralho.remove(baralho.get(chaves.get(carta)));
        io.mostrarMao(cartasSelecionadas);
        io.mostrarPontuacaoAtual(somaPontos);
    }

    private void limparMao(){
        cartasSelecionadas.clear();
        somaPontos = 0;
    }

    private int somarPontuacao(int pontuacao){
        somaPontos += pontuacao;
        return somaPontos;
    }

    public boolean validarLimite(int somaPontos){
        boolean ok = true;
        if(somaPontos == 21){
            io.blackjack();
            ok = false;
        }
        else if (somaPontos > 21){
            io.perdeu();
            ok = false;
        }
        return ok;
    }

    public int historico(int pontuacao) {
        if (pontuacao > melhorPontuacao && pontuacao <= 21) {
            melhorPontuacao = pontuacao;
        }
        return melhorPontuacao;
    }
}
