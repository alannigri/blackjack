package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public int pergunta() {
        boolean darCarta = true;
        System.out.println("'1' para nova carta ou '2' para parar");
        int resposta = scanner.nextInt();
        return resposta;
    }

    public int jogarDeNovo() {
        boolean darCarta = true;
        System.out.println("Deseja jogar novamente? ('1' para jogar '2' para sair)");
        int resposta = scanner.nextInt();
        return resposta;
    }

    public void mostrarMao(HashMap<String, Integer> cartasSelecionadas) {
        System.out.println(cartasSelecionadas);
    }

    public void mostrarPontuacaoAtual(int pontos){
        System.out.println(pontos + " pontos até o momento");
    }

    public void mostrarPontuacaoFinal(int pontos){
        System.out.println("Voce fez " + pontos + " pontos nesse jogo");
    }

    public void blackjack(){
        System.out.println("BLACKJACK!!!!");
    }

    public void perdeu(){
        System.out.println("Voce estourou os pontos!");
    }


    public void mostrarHistorico(int pontos){
        System.out.println("A sua melhor pontuação é: " + pontos);
    }



}
