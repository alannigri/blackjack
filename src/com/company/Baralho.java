package com.company;

import java.util.HashMap;

public class Baralho {
    HashMap<String, Integer> baralho = new HashMap<>();
    String[] naipe = {"Copas", "Paus", "Ouros", "Espadas"};
    String[] cartas = {"Ás", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"};

    public HashMap<String, Integer> criarBaralho() {
        for (int i = 0; i < 13; i++) {
            for (int y = 0; y < 4; y++) {
                if (i == 10 || i == 11 || i == 12) {
                    baralho.put(cartas[i] + " de " + naipe[y], 10);
                } else {
                    baralho.put(cartas[i] + " de " + naipe[y], i + 1);
                }
            }
        }
        return baralho;
    }
}
